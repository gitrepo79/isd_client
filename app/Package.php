<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Package extends Model
{
    // protected  $table = "packages";
    protected $with = ['term'];

    public function term()
    {
        return $this->belongsToMany(Term::class, 'povite_packages_terms');
    }
    
}
