<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use App\User;
use Carbon\Carbon;
use Auth;
use Session;
use DB;
use Mail;

class ProfileManageController extends Controller

{

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        //$this->middleware('auth');
    }


     public function card_manage()
    {
        
        return view('pages.managecard');
    }

}