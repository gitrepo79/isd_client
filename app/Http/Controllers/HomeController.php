<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Sport;
use App\Academy;
use App\Age;
use App\User;

use App\Product;
use App\Payment;

use Carbon\Carbon;

use App\Booking;
use App\Classes;
use App\Court;
use App\Discount;
use App\OrderProduct;
use App\Package;
use App\Subscription;
use App\Vacation;
use Auth;

use Session;

use DB;

use Mail;
use PDF;



use GuzzleHttp\Client;
use Illuminate\Support\Facades\DB as FacadesDB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\URL;


class HomeController extends Controller

{

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function index()
    {
        if (Auth::check()) {
            return redirect()->route('client.bookings');
        } else {
            return redirect('/');
        }
    }

    //Osama
    public function loadview()
    {

        //   $request->session()->forget('to');
        //     $request->session()->forget('sport');
        //     $request->session()->forget('sport_name');
        //     $request->session()->forget('from');
        //     $request->session()->forget('date');
        //     $request->session()->forget('pitch');
        //     $request->session()->forget('pitch_name');

        $cities = DB::table('cities')->get();
        $academies = Academy::all();
        $sports = Sport::pluck('name', 'id');

        $durations = [60 => '60', 90 => '90', 120 => '120'];
        $data = [];
        return view('pages.academy', ['sports' => $sports, 'durations' => $durations, 'data' => $data, 'academies' => $academies, 'cities' => $cities]);
    }

    public function getCategory(Request $request)
    {
        $academy = $request->academy;
        $dob = $request->date;
        // 11/11/2022
        $year = str_split($dob);

        // dd($year);

        // $gender = $request->gender;
        $categoryId = $this->ageCategoryValidate($year[8] . $year[9], $academy);

        // dd($categoryId);
        // $categoryName = DB::table('ages')->where('id', $categoryId)->pluck('title')->first();
        $packages = DB::table('packages')->where('age_group', $categoryId)->where('academy', $request->academy)->where('city', $request->cities)->get();
        // dd($categoryName);

        // dd($packages);

        return json_encode(array('dataResult' => $packages, 'categoryId' => $categoryId));
    }

    public function ageCategoryValidate($dob, $academy)
    {
        // dd($academy);
        if ($dob > 02) {
            $ageCategory = DB::table('ages')->where('academy', $academy)->where('start', '<=', $dob)->where('end', '>=', $dob)->pluck('id')->first();
        } else {
            $ageCategory = DB::table('ages')->where('academy', $academy)->where('start', '<=', $dob)->where('end', '>=', $dob)->pluck('id')->first();
        }

        // dd($ageCategory);

        return $ageCategory;
    }



    public function availableClasses(Request $request)
    {
        // dd($request->all());

        // $classes = DB::table('class')->get();
        // dd($classes);

        // $client = new Client();
        // $res = $client->request('GET', 'https://bookings.isddubai.com/api/v1/academy/classes/1/' . $request->academy . '/' . $request->age_category . '/');

        // $result = $res->getBody()->getContents();

        // $data = json_decode($result, true);

        // dd($data);

        $data = Classes::orderBy('id', 'desc')->where('academy', $request->academy)->where('age_group', $request->age_category)->get();
        // return response()->json([
        //     'status'=>200,
        //     'data'=>$data
        // ]);

        $classes = $data;

        // dd($classes);

        return view('pages.availableClasses', compact('classes'));
    }

    public function getPrice(Request $request)
    {

        // $client = new Client();
        // $res = $client->request('POST', 'https://bookings.isddubai.com/api/v1/academy/calculate/amount', [
        //     'form_params' => [
        //         'sport' => $request->sport,
        //         'package' => $request->package,
        //         'name' => $request->name,
        //         'age' => $request->age,
        //         'terms' => $request->terms,
        //         'num_day' => $request->num_day,
        //         'startDate' => $request->startDate,
        //     ]
        // ]);
        // $response = $response->getBody()->getContents();
        // $result = $res->getBody()->getContents();

        // return $result;
    }



    public function submitAcademy(Request $request)
    {
        // dd($request->all());

        $data = "[\"" . implode("','", $request->num_days) . "\"]";

        //   dd($data);

        $client = new Client();
        $res = $client->request('POST', 'https://bookings.isddubai.com/api/v1/academy/subscriptions/add', [
            'form_params' => [
                'academy' => $request->academy,
                'name' => $request->childname,
                'cost' => $request->cost,
                'startDate' => $request->start_date,
                'age' => $request->age_category,
                'package' => $request->my_checkbox,
                'type_num_days' => $request->day_num_count,
                'class' => $request->cls,
                'user_id' => Auth::user()->id,
                'num_days' => $data,
                'terms' => $request->terms,
            ]
        ]);
        //  $response = $response->getBody()->getContents();
        $result = $res->getBody()->getContents();

        // dd($result);

        return view('pages.submitAcademy');
    }

    public function all_academy_inquires()
    {

        // DB::connection()->enableQueryLog();
        $search_query = DB::table("subscriptions")
            ->leftJoin("ages", function ($join) {
                $join->on("ages.id", "=", "subscriptions.age");
            })
            ->leftJoin("packages", function ($join) {
                $join->on("packages.id", "=", "subscriptions.package");
            })
            ->leftJoin("class", function ($join) {
                $join->on("subscriptions.class", "=", "class.id");
            })
            ->select("subscriptions.*", "ages.title as age_title", "packages.package_name as package_name", "class.title as class_title")
            ->where('subscriptions.user_id', Auth::user()->id)
            ->orderByDesc('subscriptions.id')
            ->get();

        // $queries = DB::getQueryLog();
        // return dd($queries);


        // $search_query = Subscription::all();

        // dd($search_query);

        return view('pages.all-academy-inquires', ['inquires_details' => $search_query]);
    }

    public function bookings(Request $request)
    {

        $walletTotalAmnt = DB::table('wallets')->where('client_id', Auth::user()->id)->orderBy('id', 'DESC')->first();

        // dd($request);

        $request->session()->forget('to');
        $request->session()->forget('sport');
        $request->session()->forget('sport_name');
        $request->session()->forget('from');
        $request->session()->forget('date');
        $request->session()->forget('pitch');
        $request->session()->forget('pitch_name');

        $sports = Sport::pluck('name', 'id');

        $durations = [60 => '60', 90 => '90', 120 => '120'];
        $data = [];
        return view('pages.booking', ['sports' => $sports, 'durations' => $durations, 'data' => $data, 'walletTotalAmnt' => $walletTotalAmnt]);
    }

    public function all_bookings()
    {

        $search_query = Booking::where('client_id', Auth::user()->id)->orderByDesc('id')->get();

        return view('pages.allbookings', ['booking_details' => $search_query]);
    }


    public function bookings_detail($id)
    {

        $search_query = Booking::where('id', $id)->get();

        $orderProduct = OrderProduct::where('book_id', $id)->get();

        return view('pages.booking-detail', [
            'booking_details' => $search_query,
            'product_details' => $orderProduct
        ]);
    }

    public function checkSport(Request $request)
    {
        $request->session()->forget('to');
        $request->session()->forget('sport');
        $request->session()->forget('sport_name');
        $request->session()->forget('from');
        $request->session()->forget('date');
        $request->session()->forget('pitch');
        $request->session()->forget('pitch_name');



        $sports = Sport::pluck('name', 'id');

        $durations = [60 => '60', 90 => '90', 120 => '120'];
        $id = $request->sport;


        $sport_name = DB::table('sports')->where('id', $id)->value('name');
        // dd($id);
        $products = Product::where('products.stock', '!=', 0)->where('products.price', '!=', 0)->where('products.private', 1)->join('povite_sports_products', 'products.id', 'povite_sports_products.product_id')->where('povite_sports_products.sport_id', $id)->get();
        //  dd($products);
        $time = $request->time;
        $duration = $request->duration;

        $to = \Carbon\Carbon::parse($time)->addMinutes($duration)->format('H:i:s');
        $from = \Carbon\Carbon::parse($time);
        $date = $request->date;

        if ($request->id != 5) {

            $day = date('l', strtotime($request->date));

            $data = Court::where('sports', $id)->whereHas('Times', function ($query) use ($day, $time, $to) {

                $query->where('day', '=', $day)->whereTime('from', '<=', \Carbon\Carbon::parse($time))->whereTime('to', '>=', \Carbon\Carbon::parse($to));
            })->whereNotNull('rate')->pluck('name', 'id');
        } else {

            $data = Court::whereNotNull('rate')->where('sports', $id)->pluck('name', 'id');
        }

        if (count($data) == 0) {
            $error_status = 1;
        } else {
            $error_status = 0;
        }

        if ($request->submit == 1) {



            return view('pages.booking', ['sports' => $sports, 'data' => $data, 'request' => $request, 'sports' => $sports, 'durations' => $durations, 'error_status' => $error_status]);
        } elseif ($request->submit == 2) {

            //$this->validateBookings($request);
            $court = Court::where('id', $request->pitch)->first();

            Session::put('to', $to);
            Session::put('sport', $id);
            Session::put('sport_name', $sport_name);
            Session::put('from', \Carbon\Carbon::parse($time)->format('H:i:s'));
            Session::put('date', $request->date);
            Session::put('pitch', $request->pitch);
            Session::put('pitch_name', $court->name);

            return view('pages.court', ['products' => $products]);
        }
    }

    public function resetForm(Request $request)
    {

        $request->session()->forget('to');
        $request->session()->forget('sport');
        $request->session()->forget('sport_name');
        $request->session()->forget('from');
        $request->session()->forget('date');
        $request->session()->forget('pitch');
        $request->session()->forget('pitch_name');

        $sports = Sport::pluck('name', 'id');

        $durations = [60 => '60', 90 => '90', 120 => '120'];
        $data = [];
        return view('pages.booking', ['sports' => $sports, 'durations' => $durations, 'data' => $data]);
    }

    public function addBooking(Request $request)
    {

        $to = Session::get('to');
        $from = Session::get('from');
        $sport = Session::get('sport');
        $date = Session::get('date');
        $pitch = Session::get('pitch');
        $pitch_name = Session::get('pitch_name');

        $var = '20/04/2012';
        //$date = str_replace('/', '-', $date);
        //date_format($date,"Y-m-d");        

        $date = Carbon::parse($date);

        $date = $date->toDateString();

        $client = User::find(Auth::user()->id);

        $size = Court::where('id', $pitch)->value('size');

        $court = Court::where('id', $pitch)->first();

        $namesport = DB::table('sports')->where('id', $sport)->value('name');

        $booking = new Booking();

        $booking->date = $date;

        $booking->from = $from;

        $booking->to = $to;

        $booking->pitch = $pitch;

        $booking->size = $size;

        $booking->price = $court->rate;

        $booking->dicsount = 0;

        $booking->total = $court->rate;

        $booking->client_id = Auth::user()->id;

        $booking->status = 0;

        $booking->penalty = 0;

        $booking->sports = $sport;

        $booking->code = rand(10000, 99999);

        $booking->save();
        $product_sum = 0;
        $name = [];
        if ($request->products > 0) {
            for ($i = 0; $i < count($request->products); $i++) {
                if ($request->qty[$i] != 0) {
                    $qty = (int)$request->qty[$i];
                    $product = Product::where('id', $request->products[$i])->first();

                    $OrderProduct = new OrderProduct();

                    $OrderProduct->title = $product->id;

                    $OrderProduct->price = $product->price;

                    $OrderProduct->total = $product->price * $qty;

                    $OrderProduct->count = $qty;

                    $OrderProduct->book_id = $booking->id;

                    $OrderProduct->save();

                    $product = Product::where('id', $product->id)->first();
                    $product->decrement('stock', $qty);
                    $product->save;

                    $product_sum += (int)$product->price * $qty;
                    $name[] = $product->title . '*' . $request->qty[$i];
                }
            }
        }

        $details[$sport] = [
            'from' => $from,
            'to' => $to,
            'name' => $name,
            'product_total' => $product_sum,
            'total' => $court->rate + $product_sum,
            'pitch' => $booking->pitch,
            'sports' => $namesport,
            'court_rate' => $court->rate,
            'booking_id' => $booking->id,
            'client_id' => $booking->client_id
        ];


        $client_payment_token_name = DB::table('client_tokens')->where('user_id', $booking->client_id)->take(1)->get()->toArray();

        // print_r($client_payment_token_name[0]->token_name);
        // exit;


        // $email = DB::table('users')->where('id',$client->id)->value('email');

        // Mail::to($email)->send(new \App\Mail\Booking($details));   

        if (!empty($client_payment_token_name)) {
            $token_data = $client_payment_token_name[0]->token_name;
        } else {
            $token_data = "0";
        }


        // print_r($client_payment_token_name);
        // exit;

        return view('pages.review', ['details' => $details, 'token_name' => $token_data]);
    }

    public function token_test()
    {

        return view('pages.token_update');
    }

    public function merchant_response()
    {

        $fortParams = array_merge($_GET, $_POST);

        // dd($fortParams);

        // print("<pre>".print_r($fortParams , true)."</pre>");
        // exit;

        if ($fortParams) {
            if ($fortParams['response_message'] == "Success") {

                $paid_amount = $fortParams['amount'] / 100;

                $Payment = new Payment();

                $Payment->amount = $paid_amount;
                $Payment->ref = $fortParams['merchant_reference'];
                $Payment->status = 1;
                $Payment->description = "Payment Successful by " . $fortParams['payment_option'] . " Number " . $fortParams['card_number'];
                $Payment->rent = 0;
                $Payment->book_id = $fortParams['merchant_extra2'];
                $Payment->client_id = $fortParams['merchant_extra'];

                if (array_key_exists("token_name", $fortParams)) {
                    // $Payment->token_name  = $fortParams['token_name'];

                    // $client_token_table = DB::table('client_tokens')->select('card_number')->where('user_id', $fortParams['merchant_extra'])->get()->toArray();


                    // print("<pre>".print_r($client_token_table , true)."</pre>");
                    // exit;

                    // if(empty($client_token_table)){


                    $checkToken = DB::table('client_tokens')->where('token_name', $fortParams['token_name'])->first();

                    if ($checkToken === null) {

                        // user doesn't exist
                        DB::table('client_tokens')->insert(
                            array(
                                'user_id' => $fortParams['merchant_extra'],
                                'token_name' => $fortParams['token_name'],
                                'card_number' => $fortParams['card_number'],
                                'signature' => $fortParams['signature'],
                                'merchant_reference' => $fortParams['merchant_reference'],
                                'amount' => $fortParams['amount'],
                                'booking_id' => $fortParams['merchant_extra2'],
                                'randomnum' => $fortParams['merchant_extra3'],
                                'order_description' => $fortParams['order_description'],
                                'is_active' => 1,
                                'all_response' => json_encode($fortParams),

                            )
                        );
                    }
                }

                // else{

                //     $all_tokens = array();

                //     // print("<pre>".print_r($client_token_table , true)."</pre>");
                //     // exit;

                //     foreach($client_token_table as $key => $tokens){
                //         // print("<pre>".print_r($tokens->card_number , true)."</pre>");
                //         // $all_tokens = $tokens->card_number;
                //     }

                //     // print("<pre>".print_r($all_tokens , true)."</pre>");
                //     // exit;                            

                //  if($fortParams['card_number'] != $all_tokens){

                //     DB::table('client_tokens')->insert(
                //          array(
                //                 'user_id'     =>   $fortParams['merchant_extra'], 
                //                 'token_name'   =>   $fortParams['token_name'],
                //                 'card_number'   =>   $fortParams['card_number'],
                //                  'signature'   =>   $fortParams['signature']
                //          )
                //         );
                //     }
                // }

            }

            $Payment->save();


            if (!empty($fortParams['merchant_extra4'])) {

                $walletTotalAmnt = DB::table('wallets')->where('client_id', Auth::user()->id)->orderBy('id', 'DESC')->first();

                $final = $walletTotalAmnt->total - $fortParams['merchant_extra4'];

                $wallet = DB::table('wallets')->insert([
                    'client_id' => Auth::user()->id,
                    'debit_amount' => $fortParams['merchant_extra4'],
                    'remarks' => 'pay for booking',
                    'total' => $final,
                    'is_active' => 1,
                    'created_at' => Carbon::now()->toDateTimeString(),
                ]);
            }





            DB::table('bookings')->where('id', $fortParams['merchant_extra2'])->limit(1)->update(array('status' => 1));

            //DB::table('bookings')->where('id', $fortParams['merchant_extra2'])->first();

            $booking_data2 = DB::table('bookings')->where('id', $fortParams['merchant_extra2'])->get();

            $booking_data = Booking::where('id', $fortParams['merchant_extra2'])->get();

            // $booking_data2 = Booking::where('id', $fortParams['merchant_extra2'])->get();

            $client = $booking_data[0]->client_id;

            $size = Court::where('id', $booking_data[0]->pitch)->value('size');

            $court = Court::where('id', $booking_data2[0]->pitch)->first();

            $namesport = DB::table('sports')->where('id', $booking_data[0]->sports)->value('name');

            $OrderProduct = DB::table('order_products')->where('book_id', $booking_data[0]->id)->get();



            $product_sum = 0;
            $name = [];

            for ($i = 0; $i < count($OrderProduct); $i++) {

                $qty = $OrderProduct[$i]->count;

                $product = Product::where('id', $OrderProduct[$i]->title)->first();

                $product_sum += (int)$OrderProduct[$i]->total;
                $name[] = $product->title . '*' . $qty;
            }

            $details[$booking_data[0]->sports] = [
                'from' => $booking_data[0]->from,
                'to' => $booking_data[0]->to,
                'name' => $name,
                'product_total' => $product_sum,
                'total' => $paid_amount,
                'pitch' => $booking_data[0]->pitch,
                'sports' => $namesport,
                'court_rate' => $court->rate,
                'booking_id' => $booking_data[0]->id,
            ];

            return view('pages.thankyou', ['details' => $details]);
        } else if ($fortParams['response_message'] == "Transaction declined") {

            return view('pages.fail');
        }
    }

    public function update_token()
    {

        $fortParams = array_merge($_GET, $_POST);
        dd($fortParams);
    }




    public function getMerchantDetails(Request $request)
    {

        // $Payment = new Payment();

        $Payment = DB::table('client_tokens')->select('*')->where('user_id', $request->values)->where('is_active', 1)->groupBy('token_name')->get();

        // dd($Payment);

        // $request->values;



        return response()->json(['success' => 200, 'data' => $Payment]);
    }


    public function getMerchantToken(Request $request)
    {

        if ($request->values != null) {



            $Payment = DB::table('client_tokens')->where('id', $request->values)->where('is_active', 1)->first();


            $token_name = $Payment->token_name;
            $booking_id = $request->booking_id;

            // agr wallet se kuch amount minus hui ho to update hogi amount.
            $totalAmount = $request->totalAmount;

            $client_id = $request->client_id;
            $sports = $request->sports;
            $pitch = $request->pitch;
            $walletAmount = $request->walletAmount;
            $returnHTML = view('pages.payfort_pay', compact('token_name', 'booking_id', 'totalAmount', 'client_id', 'sports', 'pitch', 'walletAmount'))->render();

            return response()->json(['success' => 200, 'data' => $token_name, 'html' => $returnHTML]);
        } else {



            $token_name = null;
            $booking_id = $request->booking_id;
            $totalAmount = $request->totalAmount;
            $client_id = $request->client_id;
            $sports = $request->sports;
            $pitch = $request->pitch;
            $walletAmount = $request->walletAmount;
            $returnHTML = view('pages.payfort_pay', compact('token_name', 'booking_id', 'totalAmount', 'client_id', 'sports', 'pitch', 'walletAmount'))->render();

            return response()->json(['success' => 200, 'data' => $token_name, 'html' => $returnHTML]);
        }
    }


    public function emailVerification($user)
    {
        return view('pages.confirm', ['user' => $user]);
    }

    private function validateBookings(Request $request)
    {

        $request->validate([
            'pitch' => 'required',
        ]);
    }

    public function ExportInvoice($id)
    {
        $booking = Booking::find($id);
        $details = [
            'created_at' => $booking->created_at,
            'date' => $booking->date,
            'from' => $booking->from,
            'to' => $booking->to,
            'pitch' => $booking->pitch,
            'sports' => $booking->sports,
            'total' => $booking->total,
            'client_id' => $booking->client_id,
            'code' => $booking->code,
        ];
        $pdf = PDF::loadView('emails.tax', compact('details'));

        print("<pre>" . print_r($pdf, true) . "</pre>");

        return $pdf->download('invoice' . $id . '.pdf');
    }

    public function ExportReceipt($id)
    {
        $booking = Booking::find($id);
        $name = DB::table('users')->where('id', $booking->client_id)->value('name');
        $email = DB::table('users')->where('id', $booking->client_id)->value('email');
        $details = [
            'code' => $booking->code,
            'date' => $booking->date,
            'from' => $booking->from,
            'to' => $booking->to,
            'pitch' => $booking->pitch,
            'sports' => $booking->sports,
            'total' => $booking->total,
            'name' => $name,
            'email' => $email,
            'date' => $booking->created_at,
        ];
        $pdf = PDF::loadView('emails.receipt', compact('details'));
        return $pdf->download('receipt' . $id . '.pdf');
    }


    public function allCards()
    {

        $cards = DB::table('client_tokens')->where('user_id', Auth::user()->id)->where('is_active', 1)->groupBy('token_name')->get();
        return view('pages.all-cards', ['cards' => $cards]);
    }

    public function cardDelete(Request $request)
    {

        // dd($request->all());


        $returnurl2 = URL::to('/');

        $shaString = '';

        // $arrData    = array(
        // 'command'            =>'AUTHORIZATION',
        // 'access_code'        =>'GktW1EwizGB7tZ51ocEG', 
        // 'merchant_identifier'=>'LmIAIBAK',
        // 'merchant_reference' =>$request->randomnum,
        // 'amount'             =>$request->amount,
        // 'currency'           =>'AED',
        // 'language'           =>'en',
        // 'merchant_extra'     =>$request->client_id,
        // 'merchant_extra2'    =>$request->booking_id,
        // 'merchant_extra3'    =>$request->randomnum,
        // 'customer_email'     =>'test@payfort.com',
        // 'order_description'  =>$request->order_description,
        // 'return_url'         =>$returnurl2.'/client/response/bookings',
        // 'token_name'         =>$request->token_name
        // );

        $arrData = array(
            'service_command' => 'UPDATE_TOKEN',
            'access_code' => 'GktW1EwizGB7tZ51ocEG',
            'merchant_identifier' => 'LmIAIBAK',
            'merchant_reference' => $request->randomnum,
            'language' => 'en',
            'token_name' => $request->token_name,
            'token_status' => 'INACTIVE',
        );


        ksort($arrData);


        foreach ($arrData as $key => $value) {
            $shaString .= "$key=$value";
        }



        $shaString = "83/cPmhQ/ub0O3i8z0Qp4j-[" . $shaString . "83/cPmhQ/ub0O3i8z0Qp4j-[";


        $signature = hash("sha256", $shaString);

        //   dd($signature);
        // print("<pre>".print_r($arrData,true)."</pre>");  

        //   print("<pre>".print_r( $request->token_name,true)."</pre>");

        //   print("<pre>".print_r($signature,true)."</pre>");


        //   print("<pre>".print_r($signature,true)."</pre>");

        $requestParams = array(
            'service_command' => 'UPDATE_TOKEN',
            'access_code' => 'GktW1EwizGB7tZ51ocEG',
            'merchant_identifier' => 'LmIAIBAK',
            'merchant_reference' => $request->randomnum,
            'language' => 'en',
            'signature' => $signature,
            'token_name' => $request->token_name,
            'token_status' => 'INACTIVE',
        );


        $json = json_encode($requestParams);


        // dd($json);




        $client = new Client();

        $headers = [
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
        ];



        $res = $client->request('POST', 'https://sbpaymentservices.payfort.com/FortAPI/paymentApi', [
            'headers' => $headers,
            'body' => $json
        ]);
        $result = $res->getBody()->getContents();


        // dd($result);

        $cards = DB::table('client_tokens')
            ->where('token_name', $request->token_name)
            ->update(['is_active' => 0]);

        return redirect()->back()->with('success', 'Card has been removed!');
    }


    public function processWallet(Request $request)
    {

        $id = $request->client_id;
        $booking_amount = $request->totalAmount;

        // dd($request->all());

        $walletChecking = DB::table('wallets')->where('client_id', $request->client_id)->exists();

        if ($walletChecking) {

            // dd('exists');

            $walletTotalAmnt = DB::table('wallets')->where('client_id', $id)->orderBy('id', 'DESC')->first();

            $walletAmount = $walletTotalAmnt->total;

            // if wallet balance is low -> iski condition hai abi use ni ho rahi.
            // if ($walletAmount >= $booking_amount) {

            $final = $booking_amount - $walletAmount;

            if ($walletAmount >= $booking_amount) {

                $data = [
                    'bookingTotalAmount' => $booking_amount,
                    'lessWalletAmount' => $booking_amount,
                    'finalAmount' => 0
                ];
            } else {
                $data = [
                    'bookingTotalAmount' => $booking_amount,
                    'lessWalletAmount' => $walletAmount,
                    'finalAmount' => $final
                ];
            }

            // if($final == 0){
            //     return response()->json(['success' => 100, 'data' => $data]);
            // }else{
            return response()->json(['success' => 200, 'data' => $data, 'message' => "You're Wallet balance has been credited with AED " . $data["lessWalletAmount"] . ""]);
            // }

            // }else{

            //     return response()->json(['success' => 300, 'data' => 'Your wallet amount is too low!']);

            // }

        } else {

            return response()->json(['success' => 500, 'message' => 'Your wallets does not exist!']);
        }

        return false;
    }

    public function processWalletSuccess(Request $request)
    {

        $walletTotalAmnt = DB::table('wallets')->where('client_id', Auth::user()->id)->orderBy('id', 'DESC')->first();

        $final = $walletTotalAmnt->total - $request->lessWalletAmount;

        $wallet = DB::table('wallets')->insert([
            'client_id' => Auth::user()->id,
            'debit_amount' => $request->lessWalletAmount,
            'remarks' => 'pay for booking',
            'total' => $final,
            'is_active' => 1,
            'created_at' => Carbon::now()->toDateTimeString(),
        ]);


        $user = DB::table('users')->where('id', Auth::user()->id)->first();



        DB::table('payment')->insert([
            'amount' => $request->booking_amount,
            'type' => 3, // client wallet
            'book_id' => $request->booking_id,
            'status' => 1,
            'description' => 'paid by wallet',
            'client_id' => Auth::user()->id,
            'created_at' => Carbon::now()->toDateTimeString(),
            'client_email' => $user->email,
            'client_mobile' => $user->contacts,
            'client_name' => $user->name,

        ]);

        DB::table('bookings')->where('id', $request->booking_id)->limit(1)->update(array('status' => 1));


        return response()->json(['success' => 200, 'message' => 'success!']);


        // dd($request->lessWalletAmount);

    }

    public function thankyou(Request $request)
    {


        $booking_data2 = DB::table('bookings')->where('id', $request->booking_id)->get();

        $booking_data = Booking::where('id', $request->booking_id)->get();

        // $booking_data2 = Booking::where('id', $fortParams['merchant_extra2'])->get();

        $client = $booking_data[0]->client_id;

        $size = Court::where('id', $booking_data[0]->pitch)->value('size');

        $court = Court::where('id', $booking_data2[0]->pitch)->first();

        $namesport = DB::table('sports')->where('id', $booking_data[0]->sports)->value('name');

        $OrderProduct = DB::table('order_products')->where('book_id', $booking_data[0]->id)->get();

        $product_sum = 0;
        $name = [];

        for ($i = 0; $i < count($OrderProduct); $i++) {

            $qty = $OrderProduct[$i]->count;

            $product = Product::where('id', $OrderProduct[$i]->title)->first();

            $product_sum += (int)$OrderProduct[$i]->total;
            $name[] = $product->title . '*' . $qty;
        }

        $details[$booking_data[0]->sports] =
            [
                'from' => $booking_data[0]->from,
                'to' => $booking_data[0]->to,
                'name' => $name,
                'product_total' => $product_sum,
                'total' => $request->booking_amount,
                'pitch' => $booking_data[0]->pitch,
                'sports' => $namesport,
                'court_rate' => $court->rate,
                'booking_id' => $booking_data[0]->id,
            ];

        return view('pages.thankyou',  ['details' => $details]);
    }



    // Without API
    public function UserDetails(Request $request)
    {
        $user = User::where('id', $request->id)->with('children')->first();
        $response['status'] = 200;
        $response['data'] = $user;
        return $response;
    }
    public function Terms()
    {
        $data = FacadesDB::table('terms')->get();
        return response()->json([
            'status' => 200,
            'data' => $data
        ]);
    }

    public function MyPackages(Request $request)
    {

        // dd($request->all());
        // exit;

        if ($request->terms != null) {

            $firstTermID = array_values($request->terms)[0];
            $endTermID = array_reverse($request->terms)[0];

            // dd($firstTermID."<br>".$endTermID);

            $startDate = FacadesDB::table('terms')->where('id', $firstTermID)->value('start_date');
            $endtDate = FacadesDB::table('terms')->where('id', $endTermID)->value('end_date');
            $terms = trim(preg_replace('/\s*\([^)]*\)/', '', json_encode($request->terms)));

            // dd($terms);

            $myage = Age::where('academy', $request->academy)->where('start', '<=', $request->years)->where('end', '>=', $request->years)->value('id');

            // dd($myage);

            $data = FacadesDB::table('packages')->where('academy', $request->academy)->where('terms', $terms)->where('city', $request->city)->where('age_group', $myage)
                ->where(function ($query) {
                    $query->where('day1_active', 1)
                        ->orWhere('day2_active', 1)
                        ->orWhere('day3_active', 1)
                        ->orWhere('day4_active', 1)
                        ->orWhere('day5_active', 1)
                        ->orWhere('day6_active', 1)
                        ->orWhere('day7_active', 1);
                })
                ->get();

            // dd($data);
            
            if($request->academy == 1){
                 $classes = Classes::where('academy', $request->academy)->where('age_group', $myage)->where('title', 'LIKE', '%Dev%')->take(2)->get();
            }else{
                 $classes = Classes::where('academy', $request->academy)->where('age_group', $myage)->take(1)->get();
            }

    
            
            // dd($classes);
            
            return response()->json([
                'status' => 200,
                'data' => $data,
                'startDate' => $startDate,
                'lastDate' => $endtDate,
                'classes' => $classes
            ]);
        }
        return response()->json([
            'status' => 500,
            'data' => '',
            'startDate' => '',
            'lastDate' => '',
            'classes' => '',
        ]);
    }

    public function CalculateAmount(Request $request)
    {
        $package = Package::find($request->package)->toArray();



        $session_price = $package['price'];
        $firstTerm = array_values($package['term'])[0];
        $lastTerm = $package['term'][count($package['term']) - 1];

        // $data = explode(' ', $package['terms']);
        // // $endTermID = array_reverse($request->terms)[0];


        // dd($package['term']);

        $package_start = strtotime($firstTerm['start_date']);
        $package_end = strtotime($lastTerm['end_date']);

        // dd($package_start);

        $numDays = $request->num_day;
        $startDate = date('Y-m-d', strtotime($request->startDate));
        $user_start_data = strtotime($startDate);
        $package_price = $package['day' . $request->num_day];
        $TrainingWeeks = (date($package_end) - date($package_start)) / 604800;
        $TrainingSession = $TrainingWeeks * $numDays;
        $vacations = Vacation::where('strart', '>=', $startDate)->where('end', '<=', $lastTerm['end_date'])->get();
        $days = 0;
        $vacationsTotalDay = 0;
        for ($i = 0; $i < count($vacations); $i++) {
            $datetime1 = strtotime($vacations[$i]['strart']);
            $datetime2 = strtotime($vacations[$i]['end']);
            $days = (int)(($datetime2 - $datetime1) / 86400);
            $vacationsTotalDay += $days;
        }
        $numBreakDay = $vacationsTotalDay / 7;
        $numBreakDay = number_format((float)$numBreakDay, 0, '.', '');

        $DayName =  date('D', strtotime($startDate));
        if ($DayName == "Tue") {
            $daysRemaning  = ceil((date($package_end) - date($user_start_data)) / 86400 / 7);
            $sessionRemaing =  number_format((float)(($daysRemaning * $numDays) - 1) - ($numBreakDay * $numDays), 0, '.', '');
        } else {
            $daysRemaning  = ceil((date($package_end) - date($user_start_data)) / 86400 / 7);
            $sessionRemaing =  number_format((float)($daysRemaning * $numDays) - ($numBreakDay * $numDays), 0, '.', '');
        }

        $cost = $sessionRemaing * $session_price;
        $totalDiscount = $this->CalculateDiscount($request->user, $cost);
        $totalwithdiscount = $cost - $totalDiscount;
        return response()->json([
            'status' => 200,
            'sessionRemaing' => $sessionRemaing,
            'session_price' => number_format((float)$session_price, 0, '.', ''),
            'package_price' => number_format((float)$package_price, 0, '.', ''),
            'cost' => number_format((float)$cost, 2, '.', ''),
            'discount' => $totalDiscount,
            'totalwithdiscount' => number_format((float)$totalwithdiscount, 2, '.', ''),

        ]);
    }


    public function CalculateDiscount($id, $cost)
    {
        $count = Subscription::where('user_id', $id)->where('status', 1)->orderBy('subscription_fees', 'ASC')->count();
        if ($count > 0) {
            $data = Subscription::where('user_id', $id)->where('status', 1)->orderBy('subscription_fees', 'ASC')->first('subscription_fees')->toArray();
            $MinCost = $data['subscription_fees'];
            $subtotal =  $MinCost < $cost ? $MinCost : $cost;
            $percent = Discount::where('number', $count)->value('discount');
            if ($percent === null) {
                $percent = 0;
            }
            return $totalDiscount =  ($percent / 100) * $subtotal;
        } else {
            return $totalDiscount =  0;
        }
    }

    public function StoreSubscriptions(Request $request)
    {

        // dd($request->all());
        // 2015-02-17
        // dd(Carbon::parse($request->dob)->format('Y-m-d'));

        // $data = "[\"" . implode("','", $request->num_days) . "\"]";

        foreach ($request->cls as $key => $value) {
            $count = Subscription::where('class', $value)->count();
            $capacity = Classes::find($value)->first();
            $i = $capacity['capacity'] - $count;
        }



        $Subscription = new Subscription();
        $Subscription->academy = $request->academy;
        $Subscription->user_id = Auth::user()->id;
        $Subscription->start_date = $request->start_date;
        // $Subscription->enf_date = $request->enf_date;
        $Subscription->subscription_fees  = $request->cost;
        $Subscription->age = $request->age_category;
        // $Subscription->class = $request->cls;
        // $Subscription->num_days = $data;
        $Subscription->terms = $request->terms;
        $Subscription->package  = $request->my_checkbox;
        $Subscription->type_num_days  = $request->day_num_count;
        $Subscription->city = $request->cities;
        $Subscription->date_birth = $request->dob;
        $Subscription->save();

        foreach (json_decode($request->terms) as $key => $value) {
            FacadesDB::table('povite_subscriptions_terms')->insert(
                ['subscription_id' => $Subscription->id, 'term_id' => $value]
            );
        }

        // dd($request->all());

        if($request->day_num_count > 2){
            
            $myage = Age::where('academy', $request->academy)->where('start', '<=', $request->age_category)->where('end', '>=', $request->age_category)->value('id');

            $classes = Classes::where('academy', $request->academy)->where('age_group', $myage)->take($request->day_num_count)->get();


                // dd($classes);

            foreach ($classes as $key => $value) {

                // dd($value);
                FacadesDB::table('povite_subscriptions_classes')->insert(
                ['subscription_id' => $Subscription->id,
                    'classes_id' => $value->id]
                );
            }


        }else{
            foreach ($request->cls as $key => $value) {
                FacadesDB::table('povite_subscriptions_classes')->insert(
                ['subscription_id' => $Subscription->id,
                    'classes_id' => $value]
                );
                // print_r($value."<br>");
            }
        }
        
        // exit;

        

        $SubscriberCheck      = User::where('id', $request->childname)->count();
        if ($SubscriberCheck == 0) {
            $phone = User::where('id', Auth::user()->id)->value('contacts');
            $user = new User();
            $user->relative = Auth::user()->id;
            $user->name = $request->childname; // this is actually name
            $user->type = 2;
            $user->subscription = $Subscription->id;
            $user->academy = $request->academy;
            $user->email = $request->childname . '' . $request->user . "2021@gmail.com";
            $user->password = Hash::make("123456");
            $user->age_group = $request->age_category;
            $user->date = $request->dob;
            $user->contacts = $phone;
            $user->save();

            $Subscription = Subscription::find($Subscription->id);
            $Subscription->subscriber = $user->id;
            $Subscription->save();
        } else {
            $Subscription = Subscription::find($Subscription->id);
            $Subscription->subscriber = $request->childname; // this is actually id
            $Subscription->save();
        }


        // if($request->status == 1){
        //      $this->SendEmailClient(Auth::user()->id,$Subscription->id);
        // }

        // return response()->json([
        //     'status'=>200,
        //     'data' => $Subscription,
        // 'msg' => "Data Insert Success",
        // ]);      

        return view('pages.submitAcademy');
    }

    // public function SendEmailClient($id,$Subscription){
    //     $email =  DB::table('users')->where('id',$id)->value('email');
    //     Mail::to($email)->send(new \App\Mail\NewSubscription($Subscription));  
    // }
}
