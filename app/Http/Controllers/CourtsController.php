<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CourtTime;
use App\Court;
use DateTime;
use App\Court_books;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Events\BreadDataAdded;
use App\Court_times;
use Carbon;
use App\Booking;

class CourtsController  extends \TCG\Voyager\Http\Controllers\VoyagerBaseController
{

    public function store(Request $request)
    {
        if($request->parent > 0){
            $allSize = Court::where('parent',$request->parent)->sum('size');
            if($allSize >= 100){
                $data =
                [
                    'message'    => "you can't add more courts over 100%",
                    'alert-type' => 'error',
                ];
            return redirect()->back()->with($data);              
            }
        }       
        $slug = $this->getSlug($request);
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();
        $this->authorize('add', app($dataType->model_name));
        $val = $this->validateBread($request->all(), $dataType->addRows)->validate();
        $data = $this->insertUpdateData($request, $slug, $dataType->addRows, new $dataType->model_name());
        event(new BreadDataAdded($dataType, $data));  
            foreach($request->day as $row => $name){
                $day = $name;
                $from = $_POST['from'][$row];
                $to = $_POST['to'][$row];
                $court_times = new Court_times();
                $court_times->day = $day;
                $court_times->from = $from;
                $court_times->to = $to;
                $court_times->court_id = $data->id;
                $court_times->save();
            }  
            return redirect('admin/courts');
    }
    public function CheckTime(Request $request ,$id)
    {    
        $check = CourtTime::where('day',$id)->with('court')->get();
        return response()->json([
            'state'=>true,
            'msg' => 200,
            'data'=>$check
        ]);
    }

    public function CheckSport(Request $request)
    {   
        $check1 = CourtTime::where('day',$request->day)
                            ->whereTime('from','<=',\Carbon\Carbon::parse($request->from))
                            ->whereTime('to','>=',\Carbon\Carbon::parse($request->to))
                            ->pluck('court_id');
        $check = Court::where('sports',$request->sport)->whereIn('id', $check1)->get();
        return response()->json([
            'state'=>200,
            'msg' => true,
            'data'=>$check
        ]);
    }

}
