<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CourtTime;
use App\Court;
use DateTime;
use App\Court_books;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Events\BreadDataAdded;
use App\Court_times;
use DB;
use TCG\Voyager\Events\BreadDataDeleted;

class CategoriesController  extends \TCG\Voyager\Http\Controllers\VoyagerBaseController
{

    public function destroy(Request $request, $id)
    {
        $slug = $this->getSlug($request);
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();
        $check = DB::table('sports')->where('category',$id)->first();
        if($check === null){
            // Init array of IDs
            $ids = [];
            if (empty($id)) {
                // Bulk delete, get IDs from POST
                $ids = explode(',', $request->ids);
            } else {
                // Single item delete, get ID from URL
                $ids[] = $id;
            }
            foreach ($ids as $id) {
                $data = call_user_func([$dataType->model_name, 'findOrFail'], $id);

                // Check permission
                $this->authorize('delete', $data);

                $model = app($dataType->model_name);
                if (!($model && in_array(SoftDeletes::class, class_uses_recursive($model)))) {
                    $this->cleanup($dataType, $data);
                }
            }

            $displayName = count($ids) > 1 ? $dataType->getTranslatedAttribute('display_name_plural') : $dataType->getTranslatedAttribute('display_name_singular');

            $res = $data->destroy($ids);
            $data = $res
                ? [
                    'message'    => __('voyager::generic.successfully_deleted')." {$displayName}",
                    'alert-type' => 'success',
                ]
                : [
                    'message'    => __('voyager::generic.error_deleting')." {$displayName}",
                    'alert-type' => 'error',
                ];

            if ($res) {
                event(new BreadDataDeleted($dataType, $data));
            }

            return redirect()->route("voyager.{$dataType->slug}.index")->with($data);
        }else{
            $data =
             [
                'message'    => "You Can't Delete Category",
                'alert-type' => 'error',
            ];
            return redirect()->route("voyager.{$dataType->slug}.index")->with($data);  
        }
    }

}
