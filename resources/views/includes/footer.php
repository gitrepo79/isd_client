<footer class="primary-footer">
	<div class="container-wrapper">

        <div class="reach-out-section">
            <h2 class="title">
                reach out.
                <img src="https://isddubai.com/assets-web/images/icons/message.svg" alt="" class="d-none d-md-block message-icon">
            </h2>

            <div class="d-md-flex align-items-center justify-content-between">
                <ul class="unstyled inline footer-links">
                    <li>
                        <a href="#">LOCATION</a>
                    </li>

                    <li>
                        <a href="/about">ABOUT</a>
                    </li>

                    <li>
                        <a href="/privacy-policy">PRIVACY POLICY</a>
                    </li>

                    <li>
                        <a href="/terms-conditions">TERMS &AMP; CONDITIONS</a>
                    </li>
                </ul>

                <ul class="unstyled inline social-links">
                    <li>
                        <a href="#">
                            <img src="https://isddubai.com/assets-web/images/icons/instagram.svg" alt="ISD Instagram">
                        </a>
                    </li>

                    <li>
                        <a href="#">
                            <img src="https://isddubai.com/assets-web/images/icons/facebook.svg" alt="ISD Facebook">
                        </a>
                    </li>
                </ul>
            </div>
        </div>

        <div class="copyright">
            <p class="title">
                &copy; isd dubai sports city. uae 2021
            </p>
        </div>
    </div>
</footer>