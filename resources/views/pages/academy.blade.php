@extends('layouts.user')
@section('content')
<style>
    /*.ui-datepicker-calendar {*/
    /*display: none;*/
    /*}*/

    /*#hide{*/
    /*    display:none;*/
    /*}*/

    .select2-container--default.select2-container--focus .select2-selection--multiple{
        height: 36px !important;
    }

    .select2-container--default .select2-selection--multiple{
        height: 36px !important;
    }
    .select2-container .select2-selection--multiple .select2-selection__rendered{
        display: block !important;
    }
</style>

<!-- Begin Page Content -->

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
{{--
<link rel="stylesheet"
    href="https://frontendscript.com/demo/jquery-timepicker-24-hour-format/dist/wickedpicker.min.css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/css/bootstrap.css" rel="stylesheet" /> --}}

{{-- <script src="//code.jquery.com/jquery-1.12.4.js"></script>
<script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script> --}}

<section class="aboutpage-section --sports-section">
    <div class="container-wrapper">

        <div class="mb-40">
            <h2 class="maintitle">Book your session at ISD!</h2>

            <p class="maindesc --big">
                Please choose your sport, pitch or court, booking day, time & duration, from the drop-down menu.
                <br>
                If you have any questions, please call us on 04 448 1555 and we will more than happy to help you.
            </p>
        </div>

        <div class="row">
            <div class="col-lg-3">
                @include('includes.sidebar-navigation')
            </div>

            <div class="col-lg-9">
                <div class="article right-side">

                    {{-- {!! Form::open(['route' => 'availableClasses', 'files' => 'true', 'method' => 'POST',
                    'enctype'=>"multipart/form-data", 'class'=>'default-form']) !!} --}}
                    {!! Form::open(['route' => 'academy.subscriptions.add', 'files' => 'true', 'method' => 'POST',
                    'enctype'=>"multipart/form-data", 'class'=>'default-form']) !!}
                    <!-- Default Card Example -->

                    @if(!empty($error_status) && $error_status==1)
                    <div class="alert --danger" role="alert">
                        <strong>Oops!</strong> No Courts is Found
                    </div>
                    @endif


                    <div class="row">
                        <div class="col-md-12">

                            <div class="control-group">
                                {!! Form::label('cities', 'City Name', ['class' => 'form-label']) !!}
                                <select id="cities" class="form-field" name="cities" required autofocus>
                                    @foreach($cities as $city)
                                    <option value="{{ $city->id }}" {{ isset($request->cities) && $request->cities ==
                                        $city->id ? 'selected' : '' }}>{{ $city->name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="control-group">
                                {!! Form::label('academy', 'Academy Name', ['class' => 'form-label']) !!}
                                <select id="academy" class="form-field" name="academy" required autofocus>
                                    @foreach($academies as $academy)
                                    <option value="{{ $academy->id }}" {{ isset($request->academy) && $request->academy
                                        == $academy->id ? 'selected' : '' }}>{{ $academy->title }}</option>
                                    @endforeach
                                </select>
                            </div>


                            <div class="control-group">
                                {!! Form::label('childname', 'Child Name', ['class' => 'form-label']) !!}
                                <select id="childname" class="form-field select2" name="childname"></select>
                                {{-- <input id="childname" type="text" class="form-field " name="childname" value=""
                                    required placeholder="Child Name" autofocus> --}}
                            </div>

                            <div class="control-group">
                                {!! Form::label('dob', 'Date of Birth', ['class' => 'form-label']) !!}
                                {{ Form::text('dob', null, ['class' => 'form-field
                                age','id'=>'date','autocomplete'=>"off",'required'=>'required']) }}
                            </div>


                            <div class="control-group">
                                {!! Form::label('term', 'Terms', ['class' => 'form-label']) !!}
                                <select id="term" class="form-field select2" multiple name="term[]" required></select>

                            </div>

                            <div class="control-group">


                                <input type="hidden" name="package_name" id="package_name" value="" />
                                <input type="hidden" name="my_checkbox" id="package_id" value="" />
                                <input type="hidden" name="terms" id="terms" value="" />
                                <input type="hidden" name="sport" id="sport" value="" />


                                <select id="select_classes" class="select2" multiple name="cls[]" style="display: none;"></select>


                    
                                {{-- // child age category --}}
                                <input type="hidden" name="age_category" id="age_category" value="" />
                                {{-- // child id --}}
                                {{-- <input type="hidden" name="child_id" id="child_id" value="" /> --}}



                                {{-- <input type="hidden" name="age" id="age" value="" /> --}}

                                {{-- <input type="hidden" name="day_num_count" id="day_num_count" value="" /> --}}

                                <button type="button" class="btn --btn-primary" name="submit" id="next">Next</button>

                            </div>

                            {{-- <div class="control-group">
                                {!! Form::label('date', 'Start Date', ['class' => 'form-label']) !!}
                                {{ Form::text('start_date', null, ['class' =>
                                'form-field','id'=>'start_date','autocomplete'=>"off",'required'=>'required']) }}
                            </div> --}}
                            <div class="control-group" id="sport-days-select">

                                {!! Form::label('numb_of_days', 'Number of days', ['class' => 'form-label', 'id' =>
                                'numb_of_days_label']) !!}
                                <select id="numb_of_days" class="form-field" name="day_num_count" required
                                    autofocus></select>

                            </div>


                            <div class="control-group" id="package-start-date">
                                {!! Form::label('date', 'Start Date', ['class' => 'form-label']) !!}
                                {{ Form::text('start_date', null, ['class' =>
                                'form-field','id'=>'start_date','autocomplete'=>"off",'required'=>'required']) }}
                            </div>



                            <div class="control-group" id="cost">

                                {!! Form::label('cost-amnt', 'Total Amount', ['class' => 'form-label']) !!}
                                <p id="cost-amnt" class="cost" style="font-weight:bold;font-size:30px;"></p>
                            </div>


                        </div>

                        <input type="hidden" id="total-amnt" name="cost">



                        <div class="control-group">
                            <button type="submit" class="btn --football fc-white" id="btn-sbmt" name="submit"
                                value="1">Submit</button>
                            {{-- <button type="button" class="btn --football fc-white" id="btn-reset" name="reset"
                                value="1">Reset</button> --}}


                            <button type="button" id="btnCalculate" class="btn --football fc-white"
                                value="1">Calculate</button>



                        </div>

                    </div>





                    {{-- <div class="col-md-12 term" id="hide">
                        <b>{!! Form::label('package_name', 'Packages Name', ['class' => 'form-label']) !!}</b>
                        <div class="control-group">
                        </div>
                    </div> --}}

                </div>


                {{-- <div class="control-group">
                    <button type="submit" class="btn --btn-primary" name="submit" id="submit" value="1">SUBMIT</button>
                    @if(count($data)!=0)
                    <button type="submit" class="btn --football fc-white" name="submit" value="2"
                        id="proceed">Proceed</button>
                    @endif
                </div> --}}

                {!! Form::close() !!}

                {{-- <form method="post" action="{{route('client.reset.form')}}" id="hiddenForm"
                    enctype="multipart/form-data" hidden>

                    <input type="hidden" value="{{$request->sport ?? null}}" name="sport">
                    <input type="hidden" value="{{$request->time ?? null}}" name="time">
                    <input type="hidden" value="{{$request->pitch ?? null}}" name="pitch">
                    <input type="hidden" value="{{$request->date ?? null}}" name="date">
                    <input type="hidden" value="{{$request->duration ?? null}}" name="duration">

                    <input type="submit" value="submit" name="submit" id="hidden-btn">
                </form> --}}

                <div class="sport-detail">
                    <div class="football-images hidden">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="bg-primary">
                                    <h5 class="fc-white ml-4 lh-large">Indoor Artificial</h5>
                                </div>
                                <a href="https://isddubai.com/assets-web/images/gallery/football/indoor-artificial.jpg"
                                    data-fancybox>
                                    <img src='{{ asset('
                                        https://isddubai.com/assets-web/images/gallery/football/indoor-artificial.jpg')}}'
                                        alt='football-sport' class='img-fluid'>
                                </a>
                            </div>

                            <div class="col-md-4">
                                <div class="bg-primary">
                                    <h5 class="fc-white ml-4 lh-large">Outdoor Artificial</h5>
                                </div>
                                <a href="https://isddubai.com/assets-web/images/gallery/football/outdoor-artificial.jpg"
                                    data-fancybox>
                                    <img src='{{ asset('
                                        https://isddubai.com/assets-web/images/gallery/football/outdoor-artificial.jpg')}}'
                                        alt='football-sport' class='img-fluid'>
                                </a>
                            </div>

                            <div class="col-md-4">
                                <div class="bg-primary">
                                    <h5 class="fc-white ml-4 lh-large">Outdoor Grass</h5>
                                </div>
                                <a href="https://isddubai.com/assets-web/images/gallery/football/grass-pitch.jpg"
                                    data-fancybox>
                                    <img src='{{ asset('
                                        https://isddubai.com/assets-web/images/gallery/football/grass-pitch.jpg')}}'
                                        alt='football-sport' class='img-fluid'>
                                </a>
                            </div>

                        </div>
                    </div>
                    <div class="tennis-images hidden">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="bg-primary">
                                    <h5 class="fc-white ml-4 lh-large">Tennis Court 1</h5>
                                </div>
                                <a href="https://isddubai.com/assets-web/images/gallery/tennis/tennis-court1.jpg"
                                    data-fancybox>
                                    <img src='{{ asset('
                                        https://isddubai.com/assets-web/images/gallery/tennis/tennis-court1.jpg')}}'
                                        alt='tennis-sport' class='img-fluid'>
                                </a>
                            </div>

                            <div class="col-md-4">
                                <div class="bg-primary">
                                    <h5 class="fc-white ml-4 lh-large">Tennis Court 2</h5>
                                </div>
                                <a href="https://isddubai.com/assets-web/images/gallery/tennis/tennis-court2.jpg"
                                    data-fancybox>
                                    <img src='{{ asset('
                                        https://isddubai.com/assets-web/images/gallery/tennis/tennis-court2.jpg')}}'
                                        alt='tennis-sport' class='img-fluid'>
                                </a>
                            </div>

                            <div class="col-md-4">
                                <div class="bg-primary">
                                    <h5 class="fc-white ml-4 lh-large">Tennis Court 3</h5>
                                </div>
                                <a href="https://isddubai.com/assets-web/images/gallery/tennis/tennis-court3.jpg"
                                    data-fancybox>
                                    <img src='{{ asset('
                                        https://isddubai.com/assets-web/images/gallery/tennis/tennis-court3.jpg')}}'
                                        alt='tennis-sport' class='img-fluid'>
                                </a>
                            </div>

                        </div>
                    </div>
                    <div class="athletics-images hidden">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="bg-primary">
                                    <h5 class="fc-white ml-4 lh-large">Athletics Track</h5>
                                </div>
                                <a href="https://isddubai.com/assets-web/images/gallery/athletics/athletics-track.jpg"
                                    data-fancybox>
                                    <img src='{{ asset('
                                        https://isddubai.com/assets-web/images/gallery/athletics/athletics-track.jpg')}}'
                                        alt='athletics-sport' class='img-fluid'>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    </div>
</section>

<script>
    $(document).ready(function(){

   
        // total cost DIV
        $('#cost').hide();



        // Sport ki base pr show hoga
        $('#sport-days-select').hide();
        $('#package-start-date').hide();


        $('#btn-sbmt').hide();
        $('#btn-reset').hide();
        $('#btnCalculate').hide();

        // button reset on calculation
        $('#btn-reset').click(function() {
        
        $('input[name="start_date"]').val('');
        // $('input[name="cls"]:checked').prop('checked', false);
        $('input[name="num_days[]"]:checked').prop('checked', false);
        $('#cost').hide();
        $('#cost-amnt').html('');
        $('#total-amnt').val('');
        
        $('#btn-sbmt').hide();
        $('#btn-reset').hide();
        $('#btnCalculate').show();
    });
    
        
  

        $('#submit').hide();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    
            $('#childname').select2({tags: true});
            termss = []
            AvilableDays = []
            var package = 0;
            var x = 0;

     
      
            $(window).load(function() {

                var id = {{ Auth::user()->id }};
                var data = {id : id}
                $.ajax({
                    url: "{{ route('user.show') }}",
                    type: "POST",
                    data: data,
                    dataType: 'json',
                    success: function(response) {

                        // age
                        $('#dob').val(response['data']['children'][0].date);
                        $('.age').val(response['data']['children'][0].date);
                        // $('#child_id').val(response['data']['children'][0].id);
                        
                        $.each(response['data']['children'], function(key, value) {   
                            $('#childname').append($("<option></option>").attr("value", value.id).text(value.name)); 
                        });                          
                       
                    }
                }); 
                
                
                $.ajax({
                    url: "{{ route('academy.terms') }}",
                    type: "GET",
                    dataType: 'json',
                    success: function(response) {
                        
                        console.log(response);

                        // age
                        // $('.age').val(response['data']['children'][0].date);
                     
                        
                        $.each(response['data'], function(key, value) {   
                            $('#term').append($("<option></option>").attr("value", value.id).text(value.name)); 
                        });                          
                       
                    }
                }); 
          
           
            });
            
            //
            $("#childname").change(function() {
                var id = $(this).find('option:selected').val();
                $.ajax({
                    url: "{{ route('user.show') }}",
                    type: "POST",
                    data: {id:id},
                    dataType: 'json',
                    success: function(response) {
                    //   console.log(response.data.date);
                       
                        // let current_datetime = new Date("2008-10-08");

                        // let formatted_date = String(current_datetime.getMonth()).padStart(2, '0') + "/" + String(current_datetime.getDate()).padStart(2, '0') + "/" + current_datetime.getFullYear();
                        // console.log(formatted_date);

                        // age
                        $('#dob').val(response.data.date);
                        $('.age').val(response.data.date);
                        // $('#child_id').val(response.data.id);
                        $('.term .control-group').empty();
                        
                var year = $("#date").val();
                var academy = $("#academy").val();
                dob = new Date(year);
                var today = new Date();
                var my_age = Math.floor((today-dob) / (365.25 * 24 * 60 * 60 * 1000));  
                $('#age_category').val(my_age);
                     
                       
                        // let current_datetime = new Date(['data']['children'][0].date);

                        // let formatted_date = String(current_datetime.getMonth()).padStart(2, '0') + "/" + String(current_datetime.getDate()).padStart(2, '0') + "/" + current_datetime.getFullYear();
                        // console.log(formatted_date);

                       
                    }
                }); 
                
            });


            // Number of days will be show
            $("#academy").bind('change', function() {

                $('#numb_of_days').empty();
                if($(this).val() == 1){

                    // alert('football');

                    // for (var i = 1; i < 3; i++) {
                        $('#numb_of_days').append('<option value="2" selected="selected">2</option>');
                    // }
                    
                    $('#sport-days-select').hide();
                    $('#numb_of_days').hide();
                    $('#numb_of_days_label').hide();

                }else if($(this).val() == 2){

                    // alert('tennis');   
                    // $('#numb_of_days').show();
                    for (var i = 1; i < 8; i++) {
                        $('#numb_of_days').append('<option value="'+i+'">'+i+'</option>');
                    }
                    $('#sport-days-select').show();
                    $('#numb_of_days').show();
                    $('#numb_of_days_label').show();
                    
                }else if($(this).val() == 3){

                    // alert('atheltes');
                    for (var i = 1; i < 8; i++) {
                        $('#numb_of_days').append('<option value="'+i+'">'+i+'</option>');
                    }
                    $('#sport-days-select').show();
                    $('#numb_of_days').show();
                    $('#numb_of_days_label').show();

                }

            });
            $('#academy').trigger('change');
            
            
            $('#term').select2();
            $("#term").change(function() {
                
                var year = $("#date").val();
                
                console.log(year);
                
                var academy = $("#academy").val();
                dob = new Date(year);
                
                var today = new Date();
                var my_age = Math.floor((today-dob) / (365.25 * 24 * 60 * 60 * 1000));  
                
                $('#age_category').val(my_age);
                
                console.log(my_age);
                
                // $('#term').each( function () {
                //     termss.push($(this).val());
                // });

                var t = $('#term').val();

                console.log(t);
                
                var data = {
                    years : my_age,
                    terms : t,
                    academy : $("#academy").val(),
                    city : $("#cities").val(),
                }
                
                console.log(data);
                $('#select_classes').empty();
                
                $.ajax({
                    url: "{{ route('academy.packages.check') }}",
                    type: "POST",
                    data: data,
                    dataType: 'json',
                    success: function(response) {
                        
                        $('.term .control-group').empty();
                        
                        console.log(response);
                        
                        console.log(response['data']);
                        if(response['data'] == null){
                            alert("please select another date and terms");
                            return;
                        }else{
                            console.log(response['data']);

                          
      
                            $.each(response['classes'], function(key, value) {   
                                $('#select_classes').append($("<option></option>").attr({value:value.id, selected:'selected'}).text(value.title)); 
                            }); 

                            var i = 0;
                            $.each(response['data'], function(key, value) {  
                                i++;
                                if(i == 1){
                                    package = value.id
                                    console.log(value);
                                    // $('.term .control-group').append('<input type="radio" id="'+ value.id +'" name="my_checkbox" value="'+ value.id +'" data-package_name="'+value.package_name.trim()+'" data-age-group="'+value.age_group+'" data-terms='+value.terms+'" data-sport="'+value.sport+'" required/><label for="' + value.id + '"> '+value.package_name+'<label/><br>');
                                    
                                        $('#package_name').val(value.package_name.trim());
                                        $('#package_id').val(value.id);
                                        $('#terms').val(value.terms);
                                        $('#sport').val(value.sport);
                                        $('#age_category').val(value.age_group);
                                    
                                        if(value.day1_active == 1){
                                        AvilableDays.push('1')
                                    }
                                    if(value.day2_active == 1){
                                        AvilableDays.push('2')
                                    }
                                    if(value.day3_active == 1){
                                        this.day3_active = true
                                        AvilableDays.push('3')
                                    }
                                    if(value.day4_active == 1){
                                        this.day4_active = true
                                        AvilableDays.push('4')
                                    }
                                    if(value.day5_active == 1){
                                        this.day5_active = true
                                        AvilableDays.push('5')
                                    }
                                    if(value.day6_active == 1){
                                        this.day6_active = true
                                        AvilableDays.push('6')
                                    }
                                    if(value.day7_active == 1){
                                        this.day7_active = true
                                        AvilableDays.push('7')
                                    }                                    
                                }else{
                                    // $('#packageName').append($("<option></option>").attr("value", value.id).text(value.package_name)); 
                                    // $('.term .control-group').append('<input type="radio" id="'+ value.id +'" name="my_checkbox" value="'+ value.id +'" data-package_name="'+value.package_name.trim()+'" data-age-group="'+value.age_group+'" data-terms='+value.terms+' data-sport="'+value.sport+'" required/><label for="' + value.id + '"> '+value.package_name+'<label/><br>');
                              
                                        $('#package_name').val(value.package_name.trim());
                                        $('package_id').val(value.id);
                                        $('terms').val(value.terms);
                                        $('sport').val(value.sport);
                                        // $('').val();
                                        // $('').val();
                                        // $('').val();

                              
                                    }
                            }); 

    
                            if(AvilableDays.length == 1){
                               x = AvilableDays[0];
                            }else{
                                // $('.type_num_days').removeClass("hidden");
                                // $.each(AvilableDays, function(key, value) {   
                                //     $('#type_num_days').append($("<option></option>").attr("value", value).text(value)); 
                                // });                                
                            }
                        } 

                    }
                });                


            });
            
            
        // $(document).on('change','input[name="my_checkbox"]',function() {
    
        //     var package_name = $("input[name='my_checkbox']:checked").data('package_name');
        //         var age_group = $("input[name='my_checkbox']:checked").data('age-group');
        //         const terms = $("input[name='my_checkbox']:checked").data('terms');
        //         var sport = $("input[name='my_checkbox']:checked").data('sport');

        //         const hello = '['+terms+']';

        //         // const myvariable = "great";

        //         // const favePoem = "My text is."+myvariable+".";

        //         // console.log(terms);

        //         $('#package_name').val(package_name);

        //         $('#age_category').val(age_group);
        //         $('#terms').val(hello);
        //         $('#sport').val(sport);


        // });
        
        var user_id = {{ Auth::user()->id }};



        $('#next').click(function() {

            if($('#term').val() == null){
                alert('please select at least one term');
            }else{
                $(this).hide();
                $('#sport-days-select').show();
                $('#package-start-date').show();
                $('#btnCalculate').show();
                

            }
        });
    

        var dateToday = new Date();
            $( "#start_date" ).datepicker({
                dateFormat: 'yy-mm-dd',
                minDate: 0
            });      
            
            
         

      
    $('#btnCalculate').click(function() {

        
        if($('input[name="start_date"]').val() == '')
        {
            alert('please select date!');
        } else{



        $.ajax({
            type: 'POST',
            url: "{{ route('academy.calculate.amount') }}",
            data: {
                // sport: $('#sport').val(),
                 package: $('#package_id').val(),
                //   name: $('#childname').val(),
                //    age: $('#age').val(),
                    // terms: $('#terms').val(),
                    num_day: $('#numb_of_days').val(),
                        startDate: $('#start_date').val(),
                      user: user_id
                    
            },
            success: function(data) {
                
                console.log(data);
                
                
                //  var  res = JSON.parse(data);
                 
                 
                //  console.log(res.status)
                
                    if(data.status == 200){
                        
                        $('#cost').show();
                        $('#btn-sbmt').show();
                        $('#btn-reset').show();
                        
                        $('#cost-amnt').html(data.cost);
                        $('#total-amnt').val(data.cost);
                        
                    }
                    
                    $('#btnCalculate').hide();
            },
            error: function(data) {
                console.log(data);
            }
        });

            
    }
      
       });
    
      
      //////////////////////////////////////////////////////
      

    $("#sport").change(function(){
        if (window.location.href.indexOf("client/check/courts") > -1) {
            $('#hidden-btn').click();
            return false;
        }
        sportImages();
    })
    
    /*On Load set sport images*/    
    sportImages();

    function sportImages(){
        let sport_value = $("#sport").val();
        
        if(typeof sport_value !== 'undefined'){
            if(sport_value === ""){
                //$(".sport-detail").html("");
            }
            else if(sport_value == "1"){
                $(".sport-detail div").removeClass('show');
                $(".sport-detail .football-images").addClass('show');
            }
            else if(sport_value == "2"){
                $(".sport-detail div").removeClass('show');
                $(".sport-detail .tennis-images").addClass('show');
            }
            else if(sport_value == "5"){
                $(".sport-detail div").removeClass('show');
                $(".sport-detail .athletics-images").addClass('show');
            }
            else{
                //$(".sport-detail").html("");
            }   
        }    
    }



        var dateToday = new Date();
        $( "#date" ).datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'yy-mm-dd',
            yearRange: "2002:2018",
            // defaultDate: "01/01/2002",
            showButtonPanel: true,
            
        });


    });

</script>


<script>
    // $(document).on('change','input[name="my_checkbox"]',function() {
    
    //     var package_name = $("input[name='my_checkbox']:checked").data('package_name');
    //     $('#package_name').val(package_name);
    
    // });

    


    // $('#date').change(function() {
    //     // var gender = $("input[name=gender]:checked").val();
        
        
        
    //     var cities = $("select#cities option").filter(":selected").val();
    //     var academy = $("select#academy option").filter(":selected").val();
    //     var date = $('#date').val();
    //     $.ajaxSetup({
    //         headers: {
    //             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    //         }
    //     });
        
    //     dob = new Date(date);
    //     var today = new Date();
    //     var my_age = Math.floor((today - dob) / (365.25 * 24 * 60 * 60 * 1000));
        
    //     console.log(my_age);


    //     $.ajax({
    //         type: 'GET',
    //         url: "{{ route('getCategory') }}",
    //         data: {
    //             cities: cities,
    //             academy: academy,
    //             date: date,
    //         },
    //         dataType: 'json',
    //         success: function(data) {
                
    //             console.log(data.categoryId);
                
    //             $('#age_category').val(data.categoryId);
                
    //             $('.term .control-group').empty();
                
    //             if(data.dataResult == ''){
    //                 // console.log('emptyy hai');
            
    //                 $('.term .control-group').html('no data found!');
            
    //                 $('#submit').attr('disabled',true);        
                    
    //             }else{
    //                 console.log('data found');
                    
    //                 $('#submit').removeAttr("disabled");
                    
    //             }
    //              $('.term').show();
    //              var obj = data.dataResult;
                 
    //              $('#terms').val(obj[0]['terms']);
    //              $('#sport').val(obj[0]['sport']);
                 
    //              obj.forEach(function(element) {
    //                  $('.term .control-group').append('<input type="radio" id="'+ element.id +'" name="my_checkbox" value="'+ element.id +'" data-package_name="'+element.package_name+'" required/><label for="' + element.id + '"> '+element.package_name+'<label/><br>');
    //             });
                

    //         },
    //         error: function(data) {
    //             console.log(data);
    //         }
    //     });
    // });
    
    
</script>

<!-- /.container-fluid -->


{{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.12.0/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.js"></script>
<script
    src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js">
</script> --}}

<script type="text/javascript">
    $(function() {
    $('#time').timepicker({
        timeFormat: 'h:mm p',
        interval: 30,
        minTime: '05:30am',
        maxTime: '11:30pm',
        dynamic: false,
        dropdown: true,
        scrollbar: true
    });
});

</script>

@endsection