@extends('layouts.app')
@section('pageClass', 'sportsdetailpg innerpage')
@section('title', 'login')
@section('content')

<section class="hero-banner --inner-banner" style="background-image: url('https://isddubai.com/assets-web/images/banners/football-venue.webp');">
</section>


<section class="aboutpage-section --sports-section">
    <div class="container-wrapper">
        <div class="row align-items-center">

            <div class="col-lg-10">
                <h2 class="maintitle">
                    <span class="fc-football">ISD Registration</span>
                </h2>
            </div>
        </div>

        <hr class="divider">
        
        <div class="content-section">
            <div class="row">
                <section class="col-xl-5 col-lg-6">
                    
                    <div class="box --registration-box">
                        <form class="default-form --registration-form">
                            <div class="control-group">
                                <input type="text" class="form-field" name="name" value="{{ old('name') }}" required placeholder="Enter Name" id="name" maxlength="80" autofocus>
                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="control-group">
                                <input id="email" type="email" class="form-field @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required placeholder="{{ __('E-Mail Address') }}" autofocus>
                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="control-group">
                                <input id="contacts" type="text" class="form-field @error('contacts') is-invalid @enderror" name="contacts" value="{{ old('contacts') }}" required placeholder="{{ __('Contact No') }}" maxlength="10" autofocus>
                                @error('contacts')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="control-group">
                                <input id="password" type="password" class="form-field @error('password') is-invalid @enderror" name="password" required  placeholder="{{ __('Password') }}">
                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="control-group">
                                <input id="password-confirm" type="password" class="form-field" name="password_confirmation" required autocomplete="new-password" placeholder="{{ __('Confirmation Password') }}">
                            </div>
                            
                            <div class="control-group">
                                <button type="button" class="btn --btn-primary" id="register">
                                {{ __('Register') }}
                                </button>
                            </div>

                            <p class="h4">If you already have an account <a href="/user-login" class="fc-football">Sign In</a></p>
                            
                        </form>
                    </div>
                </section>
            </div>
        </div>
        
        <script type="text/javascript">
        $(document).ready(function(){
        
        $('#register').click(function(){
        var name=$("#name").val();
        var email=$('#email').val();
        var password=$("#password").val();
        var contacts=$('#contacts').val();
        $.ajax({
        type: 'POST',
        url: 'https://bookingsengine.isddubai.com/api/v1/users/register',
        data: {name:name,email:email,password:password,contacts:contacts},
        dataType: 'json',
        success: function (data) {
        console.log(data);
        var status = data.status;
        var errormsg = data.msg;
        var id= data.id;
       
        if(status==200)
        {
        var url = '{{ route("user.email.verification", ":id") }}';
        url = url.replace(':id', id);
        window.location.href = url;
        }
        else if(status==500){
        alert(errormsg);
        }
        },
        error: function (data) {
        console.log(data);
        }
        });
        });
        });
        </script>
    </div>
</section>
@endsection